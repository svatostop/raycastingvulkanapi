NAME := prog.exe
INCLUDES := includes\vulkan
SRCDIR := src
SRC  := VulkanMain.cpp CreateWindow.cpp VulkanInit.cpp VkTools.cpp VulkanDebug.cpp VkPipeline.cpp VkFrameBuffer.cpp KeyHandle.cpp MapBuilder.cpp RayCaster.cpp

FLAGS := -m64 -std=c++17 -g
LINK :=  -m64 -std=c++17 -o
LIBS :=  -LE:/Vulkan/1.2.182.0/Lib -lvulkan-1 

RES := res\resource.rc
RESOBJ := res\resource.o

SRC := $(addprefix src\, $(SRC))
OBJS :=	$(SRC:.cpp=.obj)


all: $(NAME)
$(NAME): $(OBJS)
	windres -i $(RES) -o $(RESOBJ)
	g++  $(LINK) $(NAME) $(OBJS) $(LIBS)

%.obj : %.cpp
	g++ $(FLAGS) -I$(INCLUDES) -c -o $@ $< 

clean:
	del /Q /F $(OBJS) prog.exe

re: clean all
