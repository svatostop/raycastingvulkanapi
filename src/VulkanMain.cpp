#include "..\includes\VulkanMain.h"

int main(int argc, char **argv)
{
	mEngine engine;
	try
	{
		engine.hWnd = nullptr;
		engine.hInstance = GetModuleHandleA(nullptr);

		createWindow(engine);
		engine.start();
		showWindow(engine.hWnd, engine);
		engine.end();
	}
	catch  (std::exception const &ex)
	{
		std::cout << ex.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_DESTROY:
			PostQuitMessage(0);
			break ;
		 case WM_SIZE:
	    {
	        UINT width = LOWORD(lParam);
	        UINT height = HIWORD(lParam);
	        if (width == SIZE_MINIMIZED || height == SIZE_MINIMIZED)
	        {
	        		        std::cout << "jjjjjjj\n\n\n\n";

	        	ShowWindow(hWnd, SW_HIDE);
	        }
	        
	    }
	    	return 0;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
			break;
	}

	return 0;
}
