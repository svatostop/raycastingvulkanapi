#include "..\includes\VulkanMain.h"

void createWindow(mEngine &engine)
{

	WNDCLASSEX wcex = {};
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style =  CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.hInstance = engine.hInstance;
	wcex.hIcon = LoadIcon(engine.hInstance, MAKEINTRESOURCE(IDI_SLIDESHOW));
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SLIDESHOW_SM));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszClassName = TEXT("MyTestClass");

	if (!RegisterClassEx(&wcex))
	{
		throw std::runtime_error("Error: Can't register winClass!");
	}
	
	engine.hWnd = CreateWindow(wcex.lpszClassName, TEXT("My window"),WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_MINIMIZE,
            CW_USEDEFAULT, CW_USEDEFAULT,
            800, 800,
            nullptr,
            nullptr,
            engine.hInstance,
            nullptr);
	if (!engine.hWnd)
		throw std::runtime_error("Error: Can't create window!");
		
}

void showWindow(HWND &hWnd, mEngine &engine)
{
	ShowWindow(hWnd, SW_SHOWNORMAL);

	MSG msg = {};
	static int iTickTrigget = 0;
	int iTickCount;

	while (true)
	{
		if (PeekMessage(&msg, NULL, 0,0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);

			if (msg.message == WM_QUIT)
				break ;
		}
       iTickCount = GetTickCount();
       if (iTickCount > iTickTrigget)
       {
       		iTickTrigget = iTickCount + 10;

       		engine.mKeyHandle();
			engine.mDrawFrame();
       }
	}
    vkDeviceWaitIdle(engine.mGetDevice());
}