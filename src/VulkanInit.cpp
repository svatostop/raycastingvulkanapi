#include "..\includes\VulkanMain.h"

void mEngine::createInstance()
{
	VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "MyAppName";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "MyEngine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	if (!instanceExtensionsNames.empty())
	{
		if (!vkTools::checkInstanceExtensionsSupported())
			throw std::runtime_error("Vulkan: not supported required instance extensions!");

		createInfo.ppEnabledExtensionNames = instanceExtensionsNames.data();
		createInfo.enabledExtensionCount = static_cast<uint32_t>(instanceExtensionsNames.size());
	}

	if (enableValidationLayers && !vkTools::checkValidationLayerSupport())
		throw std::runtime_error("Vulkan: not supported validation layers!");

    VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
	if (enableValidationLayers)
	{
		createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		createInfo.ppEnabledLayerNames = validationLayers.data();

        vkDebug::populateDbgMsgCreateInfo(debugCreateInfo);
        createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*) &debugCreateInfo;
	}
	else
	{
		createInfo.enabledLayerCount = 0;
        createInfo.pNext = nullptr;
	}

	if (vkCreateInstance(&createInfo, nullptr, &(vkInstance)) != VK_SUCCESS)
		throw std::runtime_error("Vulkan: Error in the 'vkCreateInstance' func");

	std::cout << "Vulkan: Instance sucessfully created" << std::endl;

}

void mEngine::initWindowSurface()
{
	VkWin32SurfaceCreateInfoKHR createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	createInfo.hwnd = this->hWnd;
	createInfo.hinstance = this->hInstance;

	if (vkCreateWin32SurfaceKHR(vkInstance, &createInfo, nullptr, &surface) != VK_SUCCESS)
    	throw std::runtime_error("failed to create window surface!");
}

void mEngine::createSyncObjects()
{
	imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
    imagesInFlight.resize(swapChainImages.size(), VK_NULL_HANDLE);


    VkSemaphoreCreateInfo semaphoreInfo{};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphores[i]) != VK_SUCCESS ||
            vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]) != VK_SUCCESS ||
            vkCreateFence(device, &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS) {

            throw std::runtime_error("failed to create synchronization objects for a frame!");
        }
    }
}

void mEngine::cleanupSwapChain() 
{
    for (size_t i = 0; i < swapChainFramebuffers.size(); i++) {
        vkDestroyFramebuffer(device, swapChainFramebuffers[i], nullptr);
    }

    vkFreeCommandBuffers(device, commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());

    vkDestroyPipeline(device, graphicsPipeline, nullptr);
    vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
    vkDestroyRenderPass(device, renderPass, nullptr);

    for (size_t i = 0; i < swapChainImageViews.size(); i++) {
        vkDestroyImageView(device, swapChainImageViews[i], nullptr);
    }

    vkDestroySwapchainKHR(device, swapChain, nullptr);

    vkDestroyBuffer(device, indexBuffer, nullptr);
    vkFreeMemory(device, indexBufferMemory, nullptr);

    vkDestroyBuffer(device, vertexBuffer, nullptr);
    vkFreeMemory(device, vertexBufferMemory, nullptr);
}

void mEngine::cleanUp()
{
	if (enableValidationLayers)
        vkDebug::DestroyDebugUtilsMessengerEXT(vkInstance, debugMessenger, nullptr);
 	
	cleanupSwapChain();
    
    vkDestroyBuffer(device, indexBuffer, nullptr);
    vkFreeMemory(device, indexBufferMemory, nullptr);

    vkDestroyBuffer(device, vertexBuffer, nullptr);
    vkFreeMemory(device, vertexBufferMemory, nullptr);

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) 
    {
        vkDestroySemaphore(device, renderFinishedSemaphores[i], nullptr);
        vkDestroySemaphore(device, imageAvailableSemaphores[i], nullptr);
        vkDestroyFence(device, inFlightFences[i], nullptr);

    }
    vkDestroyCommandPool(device, commandPool, nullptr);

    vkDestroyDevice(device, nullptr);

    vkDestroySurfaceKHR(vkInstance, surface, nullptr);
	vkDestroyInstance(vkInstance, nullptr);
}



// void mEngine::getPlayerXY()
// {
// 	// std::string strMap[] = {"11111111111111",
// 	// 						"10000000000001",
// 	// 						"10p00000000001",
// 	// 						"10000000000001",
// 	// 						"10000000000001",
// 	// 						"10001111111111",
// 	// 						"10000000000001",
// 	// 						"10000000000001",
// 	// 						"10000000000001",
// 	// 						"11111111111111"}; 
// 	std::string strMap[] = {"1111111111111111111",
// 							"4000000000000000002",
// 							"4000000000000000002",
// 							"4000000000000000002",
// 							"40000000000p0000002",
// 							"4000000000000000002",
// 							"4000000000000000002",
// 							"4000000000000000002",
// 							"4000000000000000002",
// 							"3333333333333333333"}; 

// 	int i = 0;
// 	float tmp = y;


// 	Vertex pos;

// 	 x = -1.0f;
// 	 y = -1.0f;


// 	while (i < 10)
// 	{
// 		for (int j = 0; strMap[i][j]; j++)
// 		{
// 			if (strMap[i][j] == 'p')
// 			{
// 				player.playerPos = {x,y};
// 				player.oldPos = {x,y};
// 			}
// 			if (strMap[i][j] == '1')
// 			{
// 				wall1.boards.push_back({x,y, y+0.1f});
// 			}
// 				if (strMap[i][j] == '2')
// 			{
// 				float z = y + 0.1f;
// 				wall2.boards.push_back({x,y, z});
// 			}
// 				if (strMap[i][j] == '3')
// 			{
// 				wall3.boards.push_back({x,y, y+0.1f});
// 			}
// 				if (strMap[i][j] == '4')
// 			{
// 				wall4.boards.push_back({x,y, y+0.1f});
// 			}
// 			if (strMap[i][j] == '5')
// 			{
// 				wall5.boards.push_back({x,y, y+0.1f});
// 			}
// 			if (strMap[i][j] != 'p' && strMap[i][j] != '0')
// 			{
// 					pos = {{x,y},  {0.0f, 1.0f, 0.0f}};
// 					vertices.push_back(pos);
					
// 					x += 0.1f;
// 					pos = {{x,y},  {0.0f, 1.0f, 0.0f}};
// 					vertices.push_back(pos);

// 					y += 0.1f;
// 					pos = {{x,y},  {0.0f, 1.0f, 0.0f}};
// 					vertices.push_back(pos);

// 					x -= 0.1f;
// 					pos = {{x,y},  {0.0f, 1.0f, 0.0f}};
// 					vertices.push_back(pos);
					
// 					y -= 0.1f;


// 					int j = 0;
// 					while (j < 6)
// 					{
// 						if (j == 0 || j == 5)
// 							indices.push_back(last);
// 						else if (j == 2)
// 							indices.push_back(ind);
// 						else if (j == 3)
// 						{
// 							indices.push_back(ind);
// 							ind++;
// 						}
// 						else
// 						{
// 							indices.push_back(ind);
// 							ind++;
// 						}
// 						j++;
// 					}
// 					last += 4;
// 					ind = last + 1;

// 			}
// 			x += 0.1f;
// 		}
// 		i++;
// 		x = -1.0f;
// 		y += 0.1f;
// 		tmp = y;
// 	}

// 	wall1.name = "first";
// 	wall2.name = "sec";
// 	wall3.name = "third";
// 	wall4.name = "four";
// 	wall5.name = "fifth";

// 	finWalls.push_back(wall1);
// 	finWalls.push_back(wall2);
// 	finWalls.push_back(wall3);
// 	finWalls.push_back(wall4);
// 	finWalls.push_back(wall5);

// }

// bool mEngine::getRectPos(Walls &wallCheck, glm::vec2 &rays, float r)
// {
// 	std::vector<glm::vec3>::iterator it1;
	
// 		it1 = find_if(wallCheck.boards.begin(), wallCheck.boards.end(), [ &rays](glm::vec3 checkXY)
// 		{
// 			if ((fabs(checkXY.x - rays.x) < 0.05f) && (fabs(checkXY.y - rays.y) < 0.05f)
// 			|| (fabs(checkXY.x - rays.x) < 0.05f) && (fabs(checkXY.z - rays.y) < 0.05f))
// 				return true;
// 			return false;
// 		});

// 	if (it1 != wallCheck.boards.end())
// 	{

// 		wallCheck.rectEnd = {it1->x, it1->y, it1->z };

// 		return true;
// 	}
	
// 	return false;

// }

// bool mEngine::getRectPosBegin(Walls &wallCheck, glm::vec2 &rays, float r)
// {
// 	std::vector<glm::vec3>::iterator it1;

// 		it1 = find_if(wallCheck.boards.begin(), wallCheck.boards.end(), [ &rays](glm::vec3 checkXY)
// 		{
// 			if ((fabs(checkXY.x - rays.x) < 0.05f) && (fabs(checkXY.y - rays.y) < 0.05f)
// 			|| (fabs(checkXY.x - rays.x) < 0.05f) && (fabs(checkXY.z - rays.y) < 0.05f))
// 				return true;
// 			return false;
// 		});

// 	if (it1 != wallCheck.boards.end())
// 	{

// 		if (wallCheck.name == "third" || wallCheck.name == "fifth")
// 		{
// 			wallCheck.rectBegin = {it1->x, it1->y, it1->z };

// 			wallCheck.rectEnd = {wallCheck.boards[0].x , wallCheck.boards[0].y, wallCheck.boards[0].z};

// 		}
// 		if (wallCheck.name == "four")
// 		{
// 		wallCheck.rectBegin = { wallCheck.boards[wallCheck.boards.size()-1].x , wallCheck.boards[wallCheck.boards.size()-1].y, wallCheck.boards[wallCheck.boards.size()-1].z};
// 		wallCheck.rectEnd = {it1->x, it1->y, it1->z};
// 		}
// 		if (wallCheck.name == "first" || wallCheck.name == "sec")
// 		{
// 		wallCheck.rectBegin = { it1->x, it1->y, it1->z};
// 		wallCheck.rectEnd = {wallCheck.boards[wallCheck.boards.size()-1].x , wallCheck.boards[wallCheck.boards.size()-1].y, wallCheck.boards[wallCheck.boards.size()-1].z};
// 		}
// 		//wallCheck.draw = true;
// 		return true;
// 	}
	
// 	return false;

// }

// void mEngine::getRectPosEnd(Walls &wallCheck, glm::vec2 &rays, float &iii, float &lambdaAlpha, float r)
// {

// 	// std::vector<glm::vec2>::iterator it1 = find_if(wallCheck.boards.begin(), wallCheck.boards.end(), [&rays](glm::vec2 checkXY)
// 	// {
// 	// 	if ((fabs(checkXY.x - rays.x) < 0.05f) && (fabs(checkXY.y - rays.y) < 0.05f))
// 	// 		return true;
// 	// 	return false;
// 	// });

// 	// if (it1 != wallCheck.boards.end())
// 	// {
// 	// 	wallCheck.columnHeightEnd = 2.0/(float)((r)*cos(lambdaAlpha));
// 	// 	wallCheck.wXEnd = iii;
// 	// 	wallCheck.hYEnd = (0.0) - wallCheck.columnHeightEnd/(float)2;
// 	// 	wallCheck.drawEnd = true;
// 	// 	return ;
// 	// }
// 	// else
// 	// 	wallCheck.drawEnd = false;
// }

// void mEngine::drawWall(Walls &wallCheck)
// {
// 	Vertex rectPos;

// 	rectPos = {{wallCheck.wX, wallCheck.hY}, {wallCheck.r, 0.0f, 0.0f}};
// 	vertices.push_back(rectPos);

// 	rectPos = {{wallCheck.wXEnd , wallCheck.hYEnd},  {wallCheck.r, 0.0f, 0.0f}};
// 	vertices.push_back(rectPos);
	
// 	wallCheck.hYEnd += wallCheck.columnHeightEnd;

// 	rectPos = {{ wallCheck.wXEnd, wallCheck.hYEnd},  {wallCheck.r, 0.0f, 0.0f}};
// 	vertices.push_back(rectPos);

// 	wallCheck.hY += wallCheck.columnHeight;
// 	rectPos = {{wallCheck.wX, wallCheck.hY},  {wallCheck.r, 0.0f, 0.0f}};
// 	vertices.push_back(rectPos);

// 	int j = 0;
// 	while (j < 6)
// 	{
// 		if (j == 0 || j == 5)
// 			indices.push_back(last);
// 		else if (j == 2)
// 			indices.push_back(ind);
// 		else if (j == 3)
// 		{
// 			indices.push_back(ind);
// 			ind++;
// 		}
// 		else
// 		{
// 			indices.push_back(ind);
// 			ind++;
// 		}
// 		j++;
// 	}
// 	last += 4;
// 	ind = last + 1;

// }

// bool mEngine::checkRay(float checkAngle, int &numWall)
// {
// 	Vertex pos;
// 	glm::vec2 rays;
// 	float tmp = 1.0;


// 	for (float r = 0; r <= 5.0f; r += 0.03f) 
// 	{
	
// 	rays = {player.playerPos.x + (r * cos(checkAngle)), player.playerPos.y + (r * sin(checkAngle))};

// 	float lambdaAlpha = checkAngle-alpha;

// 	int k = numWall;
// 	// if (k >= finWalls.size()-1)
// 	// 	k = 0;
// 		while (k < finWalls.size())
// 		{
// 			if (getRectPosBegin(finWalls[k], rays, r+1))
// 			{
// 				numWall = k;
// 				return true ;
// 			}
// 			k++;
// 		}
// 	}
// 	return false;
// }

// void mEngine::drawRay(float checkAngle, int numWall )
// {
// 	Vertex pos;
// 	glm::vec2 rays;
// 	float tmp = 1.0;


// 	for (float r = 0; r <= 5.0f; r += 0.03f) 
// 	{
	
// 	rays = {player.playerPos.x + (r * cos(checkAngle)), player.playerPos.y + (r * sin(checkAngle))};

// 	float lambdaAlpha = checkAngle-alpha;

// 	// int k = numWall;
// 	// // if (k >= finWalls.size()-1)
// 	// // 	k = 0;
// 	// while (k < finWalls.size())
// 	// {
// 		if (getRectPosBegin(finWalls[numWall], rays, r+1))
// 		{
// 			return ;
// 		}
// 	// 	k++;
// 	// }
	
// 		pos = {{rays.x,rays.y},  {1.0f, 1.0f, 0.0f}};
// 		vertices.push_back(pos);
		
// 		rays.x += 0.1f;
// 		pos = {{rays.x,rays.y},  {1.0f, 1.0f, 0.0f}};
// 		vertices.push_back(pos);

// 		rays.y += 0.1f;
// 		pos = {{rays.x,rays.y},  {1.0f, 1.0f, 0.0f}};
// 		vertices.push_back(pos);

// 		rays.x -= 0.1f;
// 		pos = {{rays.x,rays.y},  {1.0f, 1.0f, 0.0f}};
// 		vertices.push_back(pos);

// 		int j = 0;
// 		while (j < 6)
// 		{
// 			if (j == 0 || j == 5)
// 				indices.push_back(last);
// 			else if (j == 2)
// 				indices.push_back(ind);
// 			else if (j == 3)
// 			{
// 				indices.push_back(ind);
// 				ind++;
// 			}
// 			else
// 			{
// 				indices.push_back(ind);
// 				ind++;
// 			}
// 			j++;
// 		}
// 		last += 4;
// 		ind = last + 1;
// 	}
// }

// void mEngine::drawLastRay(float checkAngle, int numWall )
// {
// 	Vertex pos;
// 	glm::vec2 rays;
// 	float tmp = 1.0;


// 	for (float r = 0; r <= 5.0f; r += 0.03f) 
// 	{
	
// 	rays = {player.playerPos.x + (r * cos(checkAngle)), player.playerPos.y + (r * sin(checkAngle))};

// 	float lambdaAlpha = checkAngle-alpha;

// 	// int k = numWall;
// 	// // if (k >= finWalls.size()-1)
// 	// // 	k = 0;
// 	// while (k < finWalls.size())
// 	// {
// 		if (getRectPos(finWalls[numWall], rays, r+1))
// 		{
// 			return ;
// 		}
// 	// 	k++;
// 	// }
	
// 		pos = {{rays.x,rays.y},  {1.0f, 1.0f, 0.0f}};
// 		vertices.push_back(pos);
		
// 		rays.x += 0.1f;
// 		pos = {{rays.x,rays.y},  {1.0f, 1.0f, 0.0f}};
// 		vertices.push_back(pos);

// 		rays.y += 0.1f;
// 		pos = {{rays.x,rays.y},  {1.0f, 1.0f, 0.0f}};
// 		vertices.push_back(pos);

// 		rays.x -= 0.1f;
// 		pos = {{rays.x,rays.y},  {1.0f, 1.0f, 0.0f}};
// 		vertices.push_back(pos);

// 		int j = 0;
// 		while (j < 6)
// 		{
// 			if (j == 0 || j == 5)
// 				indices.push_back(last);
// 			else if (j == 2)
// 				indices.push_back(ind);
// 			else if (j == 3)
// 			{
// 				indices.push_back(ind);
// 				ind++;
// 			}
// 			else
// 			{
// 				indices.push_back(ind);
// 				ind++;
// 			}
// 			j++;
// 		}
// 		last += 4;
// 		ind = last + 1;
// 	}
// }


// void mEngine::getVertexMap()
// {
// 	wall1.r = 1.0f;
// 	wall2.r =  0.8f;
// 	wall3.r =  0.5f;

// 	glm::vec2 raysTmp;
// 	glm::vec2 rays;

// 	Vertex pos;

// 	bool found = false;
// 	int numWall = 0;
// 	int lastWall = 0;

//  	const float fov = M_PI/3.; // field of view

//  	float iii = -1.0f;

//  	// if (alpha > M_PI)
//  	// 	alpha = 0;
//  	std::cout << "ALPHA ====" << alpha << '\n';
// 	float angleBeg = alpha - (fov/(float)2);
// 	float angleEnd = alpha + (fov/(float)2);


// 	std::cout << "\nPLAYER: " << player.playerPos.x << "||" << player.playerPos.y << '\n';
// 	std::cout << "FOV: " << angleBeg << "||" << angleEnd << '\n' << '\n';

// 	int tmpNum; 
// 	bool foundBeg = false;
// 	while (angleBeg < angleEnd)
// 	{
// 		foundBeg = checkRay(angleBeg,numWall);
// 		//tmpNum = numWall;

// 		if (foundBeg)
// 		{

// 			drawRay(angleBeg , numWall);

// 				std::cout << "\n\nCHECK ANGLE ====" << (finWalls[numWall].rectEnd.y -player.playerPos.y) <<  "===================="<< (finWalls[numWall].rectEnd.x - player.playerPos.x) << '\n';


// 			if (finWalls[numWall].name == "sec")
// 				angleBeg = atan((finWalls[numWall].rectEnd.z - player.playerPos.y  )/(float)(finWalls[numWall].rectEnd.x - player.playerPos.x));
// 			else
// 				angleBeg = atan((finWalls[numWall].rectEnd.y - player.playerPos.y  )/(float)(finWalls[numWall].rectEnd.x - player.playerPos.x));

// 			// if ((finWalls[numWall].rectEnd.y -player.playerPos.y) < 0  && (finWalls[numWall].rectEnd.x - player.playerPos.x) < 0)
// 			// {
// 			// 	angleBeg= angleBeg - M_PI;
// 			// }
// 			if ((finWalls[numWall].rectEnd.y -player.playerPos.y)  > 0 && (finWalls[numWall].rectEnd.x - player.playerPos.x) < 0)
// 			{
// 				angleBeg= angleBeg + M_PI;
// 				if (angleBeg > angleEnd)
// 				{
// 					angleBeg = angleEnd;
// 					drawLastRay(angleEnd,numWall );
// 					std::cout << finWalls[numWall].name << " FIN wall XY Beg: " << finWalls[numWall].rectBegin.x<< "||" <<finWalls[numWall].rectBegin.y<< "||" << finWalls[numWall].rectBegin.z << '\n';
// 					std::cout << finWalls[numWall].name << " FIN wall XY End: " << finWalls[numWall].rectEnd.x<< "||" <<finWalls[numWall].rectEnd.y<< "||" << finWalls[numWall].rectEnd.z << '\n';
					
		
// 					break ;
// 				}
// 				std::cout << "\nANGLE BEG "<< angleBeg << "==========================  ANGLE END "<< angleEnd << '\n';

// 			}

// 			drawLastRay(angleBeg, numWall);

// 				std::cout << finWalls[numWall].name << " wall XY Beg: " << finWalls[numWall].rectBegin.x<< "||" <<finWalls[numWall].rectBegin.y<< "||" << finWalls[numWall].rectBegin.z << '\n';
// 				std::cout << finWalls[numWall].name << " wall XY End: " << finWalls[numWall].rectEnd.x<< "||" <<finWalls[numWall].rectEnd.y<< "||" << finWalls[numWall].rectEnd.z << '\n';
			
// 			if (angleBeg != angleEnd)
// 			{
// 				numWall++;
// 				// if (numWall > 4)
// 				// 	numWall = 0;
// 			}

// 			//angleBeg += 0.001;

// 			foundBeg = false;
// 		}
// 		// else
// 			angleBeg += 0.0001;

// 	}
// 	//foundBeg = drawRay(angleBeg, numWall);

// 	// 	if (foundBeg)
// 	// 	{
// 	// 		if (finWalls[numWall].name == "sec")
// 	// 			angleBeg = atan((finWalls[numWall].rectEnd.z - player.playerPos.y  )/(float)(finWalls[numWall].rectEnd.x - player.playerPos.x));
// 	// 		else
// 	// 			angleBeg = atan((finWalls[numWall].rectEnd.y - player.playerPos.y  )/(float)(finWalls[numWall].rectEnd.x - player.playerPos.x));

// 	// 		if (finWalls[numWall].rectEnd.y > 0 && finWalls[numWall].rectEnd.x < 0)
// 	// 			angleBeg= angleBeg + M_PI;
// 	// 		if (finWalls[numWall].rectEnd.y < 0 && finWalls[numWall].rectEnd.x < 0)
// 	// 			angleBeg= angleBeg - M_PI;

// 	// 		if (angleBeg > angleEnd)
// 	// 		{
// 	// 			angleBeg = angleEnd;
// 	// 		}
// 	// 		//angleBeg = angleBeginCheck;
// 	// 		// foundBeg = drawRay(angleBeg, numWall);
// 	// 		// if (foundBeg)
// 	// 		// {
// 	// 		// 	numWall++;
// 	// 		// 	if (finWalls[numWall].name == "sec")
// 	// 		// 		angleBeg = atan((finWalls[numWall].rectBegin.z - player.playerPos.y  )/(float)(finWalls[numWall].rectBegin.x - player.playerPos.x));
// 	// 		// 	else
// 	// 		// 		angleBeg = atan((finWalls[numWall].rectBegin.y - player.playerPos.y  )/(float)(finWalls[numWall].rectBegin.x - player.playerPos.x));

// 	// 		// 	if (finWalls[numWall].rectBegin.y > 0 && finWalls[numWall].rectBegin.x < 0)
// 	// 		// 		angleBeg= angleBeg + M_PI;
// 	// 		// 	if (finWalls[numWall].rectBegin.y < 0 && finWalls[numWall].rectBegin.x < 0)
// 	// 		// 		angleBeg= angleBeg - M_PI;
// 	// 		// }
// 	// 		foundBeg = false;
// 	// 	}
// 	// 	// else
// 	// 	// {
// 	// 	// 	std::cout << "HHHHHHHHHHHMMMMMMMMMM\n";
// 	// 		angleBeg += 0.0001;
// 	// }

// 		// 	if (found)
// 		// 	{
// 		// 		if (finWalls[numWall].name == "sec")
// 		// 			angleBeg = atan((finWalls[numWall].rectEnd.z - player.playerPos.y  )/(float)(finWalls[numWall].rectEnd.x - player.playerPos.x));
// 		// 		else
// 		// 			angleBeg = atan((finWalls[numWall].rectEnd.y - player.playerPos.y  )/(float)(finWalls[numWall].rectEnd.x - player.playerPos.x));

// 		// 		if (finWalls[numWall].rectEnd.y > 0 && finWalls[numWall].rectEnd.x < 0)
// 		// 			angleBeg= angleBeg + M_PI;
// 		// 		if (finWalls[numWall].rectEnd.y < 0 && finWalls[numWall].rectEnd.x < 0)
// 		// 			angleBeg= angleBeg - M_PI;

				
// 		// 		//angleBeg = angleBeginCheck;
// 		// 		found = false;
// 		// 	}
// 		// }
// 			//drawRay(angleBeg, numWall + 1);

// 			//angleBeg = angleBeginCheck;
// 			//found = false;
// 		// if (angleBeg > angleEnd)
// 		// 	angleBeg = angleEnd;

// 	// 	if (found)
// 	// 	{
// 	// 		std::cout <<  "  TMP WALL == " << finWalls[numWall].name << '\n';
			
// 	// 		if (finWalls[numWall].name == "sec")
// 	// 			angleBeginCheck = atan((finWalls[numWall].rectEnd.z - player.playerPos.y  )/(float)(finWalls[numWall].rectEnd.x - player.playerPos.x));
// 	// 		else
// 	// 			angleBeginCheck = atan((finWalls[numWall].rectEnd.y - player.playerPos.y  )/(float)(finWalls[numWall].rectEnd.x - player.playerPos.x));
// 	// 		if (finWalls[numWall].rectEnd.y > 0 && finWalls[numWall].rectEnd.x < 0)
// 	// 			angleBeginCheck= angleBeginCheck + M_PI;
// 	// 		if (finWalls[numWall].rectEnd.y < 0 && finWalls[numWall].rectEnd.x < 0)
// 	// 			angleBeginCheck= angleBeginCheck - M_PI;

// 	// 		if (angleBeginCheck > angleEnd)
// 	// 		{
// 	// 			angleBeginCheck = angleEnd;
// 	// 			found = drawRay(angleBeginCheck, numWall);
// 	// 			break ;
// 	// 		}
// 	// 		found = false;
// 	// 	// 	found = drawRay(angleBeginCheck, numWall);
// 	// 	}

// 	// 	// if ((fabs(endWallEnd.x - finWalls[numWall].rectEnd.x) < 0.05f) && (fabs(endWallEnd.y - finWalls[numWall].rectEnd.y) < 0.05f))
// 	// 	// {
// 	// 	// 	finWalls[numWall].rectBegin.x = endWallBeg.x;
// 	// 	// 	finWalls[numWall].rectBegin.y = endWallBeg.y;

// 	// 	// 	std::cout << finWalls[numWall].rectBegin.x<< "! !heeey! ! " << finWalls[numWall].rectEnd.x << '\n'; 
// 	// 	// 	break;
// 	// 	// }
// 		// tmpNum = numWall;
// 		// angleBeginCheck += 0.0001;
// 	//}


// 	// for (; iii <= 1.0f; iii += 0.01f)
//  // 	{
// 	//  	angle = alpha - (iii * fov/(float)2);
// 	// 	for (float r = 0; r <= 5.0f; r += 0.03f) 
// 	// 	{
// 	// 		rays = {player.playerPos.x + (r * cos(angle)), player.playerPos.y + (r * sin(angle))};

// 	// 		float lambdaAlpha = angle-alpha;
// 	// 		if (wall1.draw == false)
// 	// 			getRectPosBegin(wall1, rays, iii, lambdaAlpha, r+1);
// 	// 		// if (wall1.draw == true)
// 	// 		//  	getRectPosEnd(wall1, rays, iii, lambdaAlpha, r+1);

// 	// 		// if (!wall2.draw)
// 	// 		// 	getRectPosBegin(wall2, rays, iii, lambdaAlpha, r+1);
// 	// 		// if (wall2.draw)
// 	// 		// 	getRectPosEnd(wall2, rays, iii, lambdaAlpha, r+1);
			
// 	// 		// if (!wall3.draw)
// 	// 		// 	getRectPosBegin(wall3, rays, iii, lambdaAlpha, r+1);
// 	// 		// if (wall3.draw)
// 	// 		// 	getRectPosEnd(wall3, rays, iii, lambdaAlpha, r+1);

// 	// 	}
// 	// }

// 	// for (iii = 1.0f; iii >= -1.0f; iii -= 0.01f)
//  // 	{
// 	//  	angle = alpha - (iii * fov/(float)2);
// 	//  	if (wall1.drawEnd == true)
// 	//  	{
// 	//  		std::cout <<'\n' << iii << '\n';

// 	//  		break ;
// 	//  	}
// 	// 	for (float r = 0; r <= 5.0f; r += 0.03f) 
// 	// 	{
// 	// 		rays = {player.playerPos.x + (r * cos(angle)), player.playerPos.y + (r * sin(angle))};

// 	// 		float lambdaAlpha = angle-alpha;
// 	// 		// if (wall1.draw == false)
// 	// 		// 	getRectPosBegin(wall1, rays, iii, lambdaAlpha, r+1);
// 	// 		// if (wall1.draw == true)
// 	// 		 	getRectPosEnd(wall1, rays, iii, lambdaAlpha, r+1);

// 	// 		// if (!wall2.draw)
// 	// 		// 	getRectPosBegin(wall2, rays, iii, lambdaAlpha, r+1);
// 	// 		// if (wall2.draw)
// 	// 		// 	getRectPosEnd(wall2, rays, iii, lambdaAlpha, r+1);
			
// 	// 		// if (!wall3.draw)
// 	// 		// 	getRectPosBegin(wall3, rays, iii, lambdaAlpha, r+1);
// 	// 		// if (wall3.draw)
// 	// 		// 	getRectPosEnd(wall3, rays, iii, lambdaAlpha, r+1);

// 	// 	}
// 	// }

// 	// iii = 1.0f;
// 	// // angle = alpha - (iii * fov/(float)2);

// 	// // for (float r = 0; r <= 5.0f; r += 0.03f) 
// 	// // {
// 	// // 	rays = {player.playerPos.x + (r * cos(angle)), player.playerPos.y + (r * sin(angle))};

// 	// // 	float lambdaAlpha = angle-alpha;
// 	// // 	getRectPosEnd(wall1, rays, iii, lambdaAlpha, r+1);
// 	// // }

// 	// // if (wall1.drawEnd == false)
// 	// // {
// 		// for (iii = 1.0f; iii >= -1.0f; iii -= 0.01f)
// 		// {
// 		// 	angle = alpha - (iii * fov/(float)2);
// 		// 	if (wall1.drawEnd == true)
// 		// 		break;
// 		// 	for (float r = 0; r <= 5.0f; r += 0.03f) 
// 		// 	{
// 		// 		rays = {player.playerPos.x + (r * cos(angle)), player.playerPos.y + (r * sin(angle))};

// 		// 		float lambdaAlpha = angle-alpha;
// 		// 		getRectPosEnd(wall1, rays, iii, lambdaAlpha, r+1);
// 		// 	}
// 		// }

// 	// //}

// 	// if (wall1.draw)
// 	// 	drawWall(wall1);
                                                     
// 	// if (wall2.draw)
// 	// 	drawWall(wall2);
// 	// if (wall3.draw)
// 	// 	drawWall(wall3);
// }

void mEngine::initVulkan() 
{
	wallBuilder = builder.getPlayer(player);
	wallFin = builder.mapBuilder(player, alpha);

	rayCaster.drawMap(wallBuilder, vertices, indices);
	rayCaster.drawRay(wallFin, player, vertices, indices);
	rayCaster.calculateWalls(wallFin, player, vertices, indices);
	
	createInstance();
	vkDebug::setupDebugMessenger(vkInstance, debugMessenger, enableValidationLayers);

	initWindowSurface();

	vkTools::pickPhysicalDevice(vkInstance, physicalDevice,surface);
	vkTools::createLogicalDevice(device, physicalDevice, surface, mQueue);
	vkTools::createSwapChain(physicalDevice, surface, device, swapChain, hWnd, swapChainImages, swapChainImageFormat, swapChainExtent);
	vkTools::createImageViews(swapChainImages,swapChainImageViews, device, swapChainImageFormat);

	myPipeline.createRenderPass(swapChainImageFormat, renderPass, device);
	myPipeline.createGraphicsPipeline(device, swapChainExtent,pipelineLayout, renderPass, graphicsPipeline);

	myFrameBuffer.createFramebuffers(swapChainImageViews,swapChainFramebuffers, renderPass,swapChainExtent,device);
	myFrameBuffer.createCommandPool(commandPool, physicalDevice, device, surface);

    createVertexBuffer();
    createIndexBuffer();

	myFrameBuffer.createCommandBuffers(commandBuffers,commandPool, device,swapChainFramebuffers, renderPass, swapChainExtent,
	graphicsPipeline,vertexBuffer, indexBuffer, indices);

	createSyncObjects();
}

uint32_t mEngine::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) 
{
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) 
	{
    	if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
    		return i;
	}
	throw std::runtime_error("failed to find suitable memory type!");
}

void mEngine::createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory)
{
	VkBufferCreateInfo bufferInfo{};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
        throw std::runtime_error("failed to create buffer!");
    }

    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

    if (vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
        throw std::runtime_error("failed to allocate buffer memory!");
    }

    vkBindBufferMemory(device, buffer, bufferMemory, 0);

}

void mEngine::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) 
{
 	VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = commandPool;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo{};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(commandBuffer, &beginInfo);

	VkBufferCopy copyRegion{};
	copyRegion.srcOffset = 0; // Optional
	copyRegion.dstOffset = 0; // Optional
	copyRegion.size = size;
	vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

	vkEndCommandBuffer(commandBuffer);
	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	vkQueueSubmit(mQueue.graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(mQueue.graphicsQueue);
	vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
}

void mEngine::createIndexBuffer() {
    VkDeviceSize bufferSize = sizeof(indices[0]) * indices.size();

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

    void* data;
    vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, indices.data(), (size_t) bufferSize);
    vkUnmapMemory(device, stagingBufferMemory);

    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer, indexBufferMemory);

    copyBuffer(stagingBuffer, indexBuffer, bufferSize);

    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}

void  mEngine::createVertexBuffer() 
{
	VkDeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

    void* data;
    vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
        memcpy(data, vertices.data(), (size_t) bufferSize);
    vkUnmapMemory(device, stagingBufferMemory);

    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory);

    copyBuffer(stagingBuffer, vertexBuffer, bufferSize);

    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}

void mEngine::recreateSwapChain() 
{

	vertices = std::vector<Vertex>();
	indices = std::vector<uint16_t>();

	rayCaster.cleanAll();
	wallFin.clear();
	builder.cleanWalls();

	wallFin = builder.mapBuilder(player, alpha);
	
	rayCaster.drawMap(wallBuilder, vertices, indices);
	rayCaster.drawRay(wallFin, player, vertices, indices);
	rayCaster.calculateWalls(wallFin, player, vertices, indices);


    vkDeviceWaitIdle(device);

    cleanupSwapChain();

    vkTools::createSwapChain(physicalDevice, surface, device, swapChain, hWnd, swapChainImages, swapChainImageFormat, swapChainExtent);
    vkTools::createImageViews(swapChainImages,swapChainImageViews, device, swapChainImageFormat);
    myPipeline.createRenderPass(swapChainImageFormat, renderPass, device);
    myPipeline.createGraphicsPipeline(device, swapChainExtent,pipelineLayout, renderPass, graphicsPipeline);
    myFrameBuffer.createFramebuffers(swapChainImageViews,swapChainFramebuffers, renderPass,swapChainExtent,device);

	createVertexBuffer();
    createIndexBuffer();
    myFrameBuffer.createCommandBuffers(commandBuffers,commandPool, device,swapChainFramebuffers, renderPass, swapChainExtent,
    	graphicsPipeline,vertexBuffer, indexBuffer,  indices);
}

void mEngine::drawFrame()
{
	uint32_t imageIndex;

	vkWaitForFences(device, 1, &inFlightFences[currentFrame], VK_TRUE, UINT64_MAX);

	VkResult result = vkAcquireNextImageKHR(device, swapChain, UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);

	if (result == VK_ERROR_OUT_OF_DATE_KHR) {
	    recreateSwapChain();
	    return;
	} else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
	    throw std::runtime_error("failed to acquire swap chain image!");
	}

    if (imagesInFlight[imageIndex] != VK_NULL_HANDLE)
        vkWaitForFences(device, 1, &imagesInFlight[imageIndex], VK_TRUE, UINT64_MAX);
    imagesInFlight[imageIndex] = inFlightFences[currentFrame];

    VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkSemaphore waitSemaphores[] = {imageAvailableSemaphores[currentFrame]};
	VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffers[imageIndex];

	VkSemaphore signalSemaphores[] = {renderFinishedSemaphores[currentFrame]};
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;
    
    vkResetFences(device, 1, &inFlightFences[currentFrame]);

	if (vkQueueSubmit(mQueue.graphicsQueue, 1, &submitInfo, inFlightFences[currentFrame]) != VK_SUCCESS)
		throw std::runtime_error("failed to submit draw command buffer!");

	VkPresentInfoKHR presentInfo{};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;

	VkSwapchainKHR swapChains[] = {swapChain};
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &imageIndex;
	presentInfo.pResults = nullptr; // Optional

	result = vkQueuePresentKHR(mQueue.presentQueue, &presentInfo);

	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebufferResized) {

        framebufferResized = false;
        recreateSwapChain();

	} else if (result != VK_SUCCESS) {
	    throw std::runtime_error("failed to present swap chain image!");
	}

	currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;

	if (nFrame == true)
	{
		// boards = std::vector<glm::vec2>();
		// boards2 = std::vector<glm::vec2>();
		// boards3 = std::vector<glm::vec2>();
		// boards4 = std::vector<glm::vec2>();
		// last = 0;
	 // 	ind = 1;
		// vertices = std::vector<Vertex>();
		// indices = std::vector<uint16_t>();
		nFrame = false;
		recreateSwapChain();

	}
	
}