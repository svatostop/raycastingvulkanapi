#ifndef VULKANMAIN_H
#define VULKANMAIN_H

#define VK_USE_PLATFORM_WIN32_KHR
#define _USE_MATH_DEFINES

#include <iostream>
#include <windows.h>
#include <stdexcept>
#include <cstdlib>
#include <vector>
#include <sstream>
#include <cmath>

#include "VulkanDebug.h"

#include "vulkan\vulkan.h"
#include "..\res\Resource.h"
#include "VkTools.h"
#include "VkPipeline.h"
#include "VkFrameBuffer.h"
#include "Utils.h"
#include "MapBuilder.h"
#include "RayCaster.h"


#define A_KEY 0x41
#define D_KEY 0x44
#define S_KEY 0x53
#define W_KEY 0x57
#define E_KEY 0x45
#define F_KEY 0x46
#define P_KEY 0x50
#define Q_KEY 0x51

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
const int MAX_FRAMES_IN_FLIGHT = 2;


class mEngine
{
public:
	HWND hWnd;
	HINSTANCE hInstance;

	void start()
	{
		initVulkan();
	}
	void end()
	{
		cleanUp();
	}
	void mDrawFrame() { drawFrame(); };
	void mKeyHandle() { keyHandle(); };


	VkDevice mGetDevice() { return getDevice();};

private:
	VkInstance vkInstance;
	VkDebugUtilsMessengerEXT debugMessenger;
	VkSurfaceKHR surface;
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	vkTools::Queue mQueue;
	VkDevice device;
	VkSwapchainKHR swapChain;

	std::vector<VkImage> swapChainImages;
	std::vector<VkImageView> swapChainImageViews;
	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;

	mPipeline myPipeline;
	VkRenderPass renderPass;
	VkPipelineLayout pipelineLayout;
	VkPipeline graphicsPipeline;

	mFrameBuffer myFrameBuffer;
	std::vector<VkFramebuffer> swapChainFramebuffers;
	std::vector<VkCommandBuffer> commandBuffers;
	VkCommandPool commandPool;

	VkBuffer vertexBuffer;
	//VkBuffer vertexBuffer2;
	VkDeviceMemory vertexBufferMemory;
	VkBuffer indexBuffer;
	VkDeviceMemory indexBufferMemory;

	std::vector<VkSemaphore> imageAvailableSemaphores;
	std::vector<VkSemaphore> renderFinishedSemaphores;
	std::vector<VkFence> inFlightFences;
	std::vector<VkFence> imagesInFlight;

	size_t currentFrame = 0;
	bool framebufferResized = false;
	bool nFrame = false;

	MapBuilder builder;
	PlayerVert player;
	RayCaster rayCaster;
	float alpha = M_PI/2;


	std::vector<Vertex> vertices;

	std::vector<uint16_t> indices;
	
	std::map<int, std::vector<glm::vec3>> wallBuilder;
	std::map<int,  std::vector<Walls>> wallFin;


	void initVulkan();
	void initWindowSurface();
	void createInstance();
	void createSyncObjects();
	void recreateSwapChain();

	void recreateColorBuffer();

	void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory);
	void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
	void  createVertexBuffer();
	void createIndexBuffer();
	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) ;

	void cleanupSwapChain();
	void cleanUp();

	void drawFrame();
	void keyHandle();
	bool checkCollision();

	VkDevice getDevice() { return device;};
};

void createWindow(mEngine &engine);
void showWindow(HWND &hWnd, mEngine &engine);

#endif