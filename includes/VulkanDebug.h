#ifndef VULKANDEBUG_H
#define VULKANDEBUG_H

#define VK_USE_PLATFORM_WIN32_KHR

#include <iostream>
#include "vulkan\vulkan.h"

namespace vkDebug
{
	void setupDebugMessenger(VkInstance vkInstance, VkDebugUtilsMessengerEXT &debugMessenger, bool enableValidationLayers);
	void populateDbgMsgCreateInfo(VkDebugUtilsMessengerCreateInfoEXT &createInfo);

		static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		    VkDebugUtilsMessageTypeFlagsEXT messageType,
		    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		    void* pUserData);


	VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) ;

	void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT &debugMessenger, const VkAllocationCallbacks* pAllocator) ;

}

#endif